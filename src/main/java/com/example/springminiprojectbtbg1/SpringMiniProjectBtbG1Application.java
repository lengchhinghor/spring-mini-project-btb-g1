package com.example.springminiprojectbtbg1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMiniProjectBtbG1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMiniProjectBtbG1Application.class, args);
    }

}
